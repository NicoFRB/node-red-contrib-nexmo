module.exports = function (RED) {
    "use strict";

    const request = require('request-promise');

    function nexmoNode(n) {
        RED.nodes.createNode(this, n);

        this.topic = n.topic;
        this.cred = RED.nodes.getNode(n.cred);
        this.auth = this.auth = RED.nodes.getNode(n.auth)

        let node = this;

        this.on('input', function (msg) {
            request({
                method: 'POST',
                uri: 'https://rest.nexmo.com/sms/json',
                body: {
                    'api_key': this.auth.credentials.api_key,
                    'api_secret': this.auth.credentials.api_secret,
                    'to': this.cred.number,
                    'from': this.cred.from,
                    'text': msg.payload
                },
                json: true // Automatically stringifies the body to JSON 
            }).then(function (parsedBody) {
                node.send({ topic: node.topic, payload: parsedBody.messages[0] });
            }).catch(function (err) {
                node.error(err);
            })
        });
    }

    RED.nodes.registerType("nexmo", nexmoNode);

}
