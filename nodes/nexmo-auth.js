module.exports = function (RED) {
    "use strict";

    function nexmoAuthNode(n) {
        RED.nodes.createNode(this, n);
        this.name = n.name;
    }

    RED.nodes.registerType("nexmo-auth", nexmoAuthNode, {
        credentials: {
            "api_key": { type: "password" },
            "api_secret": { type: "password" }
        }
    });

}
