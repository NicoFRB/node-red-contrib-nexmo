module.exports = function (RED) {
    "use strict";

    function nexmoCredentialNode(n) {
        RED.nodes.createNode(this, n);
        this.name = n.name;
        this.number = n.number;
        this.from = n.from;
    }

    RED.nodes.registerType("nexmo-credentials", nexmoCredentialNode);

}
