# node-red-contrib-nexmo

This node will let you send simple SMS using the Nexmo API

## Usage

#### Incoming message
``` json
{
    "payload" : "Here is my SMS"
}
```

#### Outcoming message

``` json
{
    "to":"<THE RECIPIENT NUMBER>",
    "message-id":"<THE MESSAGE ID>",
    "status":"0",
    "remaining-balance":"15.52540000",
    "message-price":"0.05940000",
    "network":"20810"
}
```

**/!\ : You will first need API key from Nexmo**